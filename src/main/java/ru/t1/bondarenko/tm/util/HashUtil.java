package ru.t1.bondarenko.tm.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;

public interface HashUtil {

    Logger LOGGER_LIFECYCLE = LoggerFactory.getLogger("LIFECYCLE");

    String SECRET = "123";

    Integer ITERATION = 100;

    static String salt(final String value) {
        if (value == null) return null;
        String result = value;
        for (int i = 0; i < ITERATION; i++) {
            result = md5(SECRET + result + SECRET);
        }
        return result;
    }

    static String md5(final String value) {
        if (value == null) return null;
        try {
            final MessageDigest md = MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(value.getBytes());
            final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; i++) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (final Exception e) {
            LOGGER_LIFECYCLE.error(e.getMessage());
            System.out.println("[FAIL]");
        }
        return null;
    }

}

