package ru.t1.bondarenko.tm.repository;

import ru.t1.bondarenko.tm.api.repository.IUserRepository;
import ru.t1.bondarenko.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    @Override
    public User add(final User user) {
        users.add(user);
        return user;
    }

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User findById(String id) {
        for (final User user : users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User findByLogin(String login) {
        for (final User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(String email) {
        for (final User user : users) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public User remove(User user) {
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    @Override
    public Boolean loginExists(String login) {
        for (final User user : users) {
            if (login.equals(user.getLogin())) return true;
        }
        return false;
    }

    @Override
    public Boolean emailExists(String email) {
        for (final User user : users) {
            if (email.equals(user.getEmail())) return true;
        }
        return false;
    }
}