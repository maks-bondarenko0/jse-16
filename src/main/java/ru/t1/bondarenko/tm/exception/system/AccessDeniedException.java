package ru.t1.bondarenko.tm.exception.system;

public class AccessDeniedException extends AbstractSystemException {

    public AccessDeniedException() {
        super("Error! Wrong Login or Password...");
    }

}