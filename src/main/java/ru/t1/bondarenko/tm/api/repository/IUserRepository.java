package ru.t1.bondarenko.tm.api.repository;

import ru.t1.bondarenko.tm.model.User;

import java.util.List;

public interface IUserRepository {

    User add(User user);

    List<User> findAll();

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User remove(User user);

    Boolean loginExists(String login);

    Boolean emailExists(String email);

}
