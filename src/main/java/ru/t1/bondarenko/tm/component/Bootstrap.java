package ru.t1.bondarenko.tm.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.t1.bondarenko.tm.api.repository.ICommandRepository;
import ru.t1.bondarenko.tm.api.repository.IProjectRepository;
import ru.t1.bondarenko.tm.api.repository.ITaskRepository;
import ru.t1.bondarenko.tm.api.repository.IUserRepository;
import ru.t1.bondarenko.tm.api.service.*;
import ru.t1.bondarenko.tm.command.AbstractCommand;
import ru.t1.bondarenko.tm.command.system.*;
import ru.t1.bondarenko.tm.command.project.*;
import ru.t1.bondarenko.tm.command.task.*;
import ru.t1.bondarenko.tm.command.user.*;
import ru.t1.bondarenko.tm.enumerated.Role;
import ru.t1.bondarenko.tm.enumerated.Status;
import ru.t1.bondarenko.tm.exception.system.CommandNotSupportedException;
import ru.t1.bondarenko.tm.model.Project;
import ru.t1.bondarenko.tm.model.Task;
import ru.t1.bondarenko.tm.repository.CommandRepository;
import ru.t1.bondarenko.tm.repository.ProjectRepository;
import ru.t1.bondarenko.tm.repository.TaskRepository;
import ru.t1.bondarenko.tm.repository.UserRepository;
import ru.t1.bondarenko.tm.service.*;
import ru.t1.bondarenko.tm.util.TerminalUtil;

public class Bootstrap implements IServiceLocator {

    private final static Logger LOGGER_LIFECYCLE = LoggerFactory.getLogger("LIFECYCLE");

    private final static Logger LOGGER_COMMANDS = LoggerFactory.getLogger("COMMANDS");

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);


    {
        registry(new ApplicationDeveloperInfoCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new UserChangePasswordCommand());
        registry(new UserEditCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserViewCommand());
    }

    private void registry(ApplicationDeveloperInfoCommand applicationDeveloperInfoCommand) {
    }


    public void run(final String... args) {
        if (parseArguments(args)) parseCommand("exit");
        initDemoData();
        initLogger();
        parseCommands();
    }

    private void parseCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                LOGGER_COMMANDS.info(command);
                parseCommand(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                LOGGER_LIFECYCLE.error(e.getMessage());
                System.out.println("[FATAL]");
            }
        }
    }

    private boolean parseArguments(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new CommandNotSupportedException(arg);
        abstractCommand.execute();
        return true;
    }

    private void parseCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void initDemoData() {
        userService.create("admin", "admin", Role.ADMIN);
        userService.create("first", "1111", "first@test.te");
        userService.create("second", "2222", "second@test.te");

        projectService.add(new Project("A Project", Status.COMPLETED));
        projectService.add(new Project("D Project", Status.IN_PROGRESS));
        projectService.add(new Project("C Project", Status.IN_PROGRESS));
        projectService.add(new Project("B Project4", Status.NOT_STARTED));

        taskService.add(new Task("E Task", "Task1"));
        taskService.add(new Task("A Task", "Task2"));
        taskService.add(new Task("F Task", "Task3"));
    }

    private void initLogger() {
        LOGGER_LIFECYCLE.info("*** WELCOME TO TASK MANAGER ***");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                LOGGER_LIFECYCLE.info("*** TASK MANAGER IS SHUTTING DOWN ***");
            }
        });
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}