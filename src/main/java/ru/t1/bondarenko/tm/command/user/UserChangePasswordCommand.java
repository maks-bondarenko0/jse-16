package ru.t1.bondarenko.tm.command.user;

import ru.t1.bondarenko.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand {

    public final static String DESCRIPTION = "Edit User.";

    public final static String NAME = "user-edit";


    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("Enter new Password:");
        final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }
}

