package ru.t1.bondarenko.tm.command.user;

import ru.t1.bondarenko.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractUserCommand {

    public final static String DESCRIPTION = "Register User.";

    public final static String NAME = "user-registry";


    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        final String password = TerminalUtil.nextLine();
        System.out.println("Enter email:");
        final String email = TerminalUtil.nextLine();
        getAuthService().registry(login, password, email);
    }
}