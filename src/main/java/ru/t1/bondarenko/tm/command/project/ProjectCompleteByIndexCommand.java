package ru.t1.bondarenko.tm.command.project;

import ru.t1.bondarenko.tm.enumerated.Status;
import ru.t1.bondarenko.tm.util.TerminalUtil;

public class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    public final static String DESCRIPTION = "Complete Project by Index.";

    public final static String NAME = "project-complete-by-index";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectService().changeProjectStatusByIndex(index, Status.COMPLETED);
    }
}
