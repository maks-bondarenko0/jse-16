package ru.t1.bondarenko.tm.command.project;

import ru.t1.bondarenko.tm.enumerated.Sort;
import ru.t1.bondarenko.tm.model.Project;
import ru.t1.bondarenko.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectListCommand extends AbstractProjectCommand {

    public final static String DESCRIPTION = "Show all Projects.";

    public final static String NAME = "project-list";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECTS]");
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Project> projects = getProjectService().findAll(sort);
        showProjectList(projects);
        System.out.println("[END]");
    }
}
