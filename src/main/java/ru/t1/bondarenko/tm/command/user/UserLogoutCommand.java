package ru.t1.bondarenko.tm.command.user;

public class UserLogoutCommand extends AbstractUserCommand {

    public final static String DESCRIPTION = "Logout User.";

    public final static String NAME = "logout";


    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthService().logout();
    }
}